# # Debugging
# 
# ## No authentication parameters were specified. These must be provided in order to authenticate with Google
# 
# Set SUPPLY_JSON_KEY to the path to the key
# 
# ## 500 Internal Server Error
# 
# Ensure the tag is pushed to GitLab and create the release manually (retry isn't possible)
#

require 'net/http'

default_platform(:android)

platform :android do
  desc "Build debug and test APK for screenshots"
  lane :build_and_screengrab do
    gradle(task: "assembleDebug assembleAndroidTest")
    capture_android_screenshots()
  end

  desc "Import translations and generate screenshots"
  lane :pre_release do
    gradle(task: "importTranslations importTranslationsForFastlane")
    build_and_screengrab()
  end

  desc "Deploy a new version"
  lane :release do |options|
    gradle(task: "clean assembleRelease")
    upload_to_play(options)
    upload_to_gitlab(options)
  end

  lane :upload_to_play do |options|
    upload_to_play_store(track: options[:production] ? "production" : "beta",
                         release_status: "draft",
                         apk: "app/build/outputs/apk/release/app-release.apk",
                         skip_upload_images: true,
                         mapping: "app/build/outputs/mapping/release/mapping.txt")
  end

  lane :upload_to_gitlab do |options|
    version = options[:version] ? options[:version] : gradle(task: "androidGitVersionName", flags: "--quiet", print_command: false, print_command_output: false).strip
    release_url = "https://gitlab.com/api/v4/projects/30453147/packages/generic/apk/#{version}/app-release.apk"
    release_uri = URI(release_url)

    UI.verbose("Uploading APK to #{release_url}")
    Net::HTTP.start(release_uri.host, release_uri.port, :use_ssl => true) do |http|
      req = Net::HTTP::Put.new(release_uri)
      req["Authorization"] = "Bearer #{File.read('../gitlab_token').strip}"
      req["Transfer-Encoding"] = "Chunked"
      req.body_stream = "../app/build/outputs/apk/release/app-release.apk"

      res = http.request(req)
      case res
      when Net::HTTPSuccess then
        # OK
      else
        raise FastlaneCore::Interface::FastlaneCrash, res.body
      end
    end

    gitlab_create_release(endpoint: "https://gitlab.com/api/v4/",
                          private_token: File.read('../gitlab_token').strip,
                          project_id: "30453147",
                          tag: version,
                          ref: "", # This causes 500 error if the version isn't tagged, but we can't fix it from the client.
                          name: version,
                          assets_links: [{
                                          "name": "APK",
                                          "url": release_url,
                                          "link_type": "package"}])
  end
end
